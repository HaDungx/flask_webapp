from flask import Flask, render_template
app = Flask(__name__)

#TODO Dung replace hi :name by greeting_by_hour ie chatbot say hi
@app.route('/')
def index():
    name='AI-BTX'
    return render_template("index.html", name=name)  #TODO Dung simpler code to use **locals in place of name=name

if __name__ == '__main__':
    app.run(debug=True)
